var express = require("express");
var models = require("./models/index");
var http = require("http");


module.exports = function (app) {

    //curl -i -X GET 'http://127.0.0.1:3000/api/users/1/kenneth.phang@acorncodinglab.com'
    // once this rest api is being called
    // it will make another http call out to any express server
    // which is the app2.js student need to start the app2.js on port 3001
    // get --> 3000 ---> timeout for 3000 ms call 3001 /asyncHook
    app.get("/api/users/:id/:email", function (req, res) {
        models.users.findOne({where: {id: req.params.id, email: req.params.email}})
            .then(function(result) {
                console.log(Date.now());
                var interval = setInterval(function() {
                    var options = {
                        host: 'localhost',
                        port: 3001,
                        method: 'POST',
                        path: "/asyncHook",
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    };

                    var request = http.request(options, function(response) {
                        response.on('data', function(d) {
                            clearInterval(interval);
                        });
                    });
                    request.write(JSON.stringify(result));
                    request.end();
                }, 3000);
                res.status(200).send("{Done: true}");
            }).catch(function (err) {
                console.error(err);
                res.status(500).send(err);
        });
    });

    function returnResults(results, res) {
        res.status(200).send(results);
    }

};
