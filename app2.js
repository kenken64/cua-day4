/**
 * Created by phangty on 10/9/16.
 */
var path = require("path");
var express = require("express");
var app = express();

var bodyParser = require("body-parser");
var session = require("express-session");
var cookieParser = require('cookie-parser');

const  PORT = "port";

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Initialize session
app.use(session({
    secret: "nus-stackup",
    resave: false,
    saveUninitialized: true
}));

// receiving end of another middleware
app.post("/asyncHook", function (req, res) {
    console.log("AsyncHook ---> " + Date.now());
    console.log(req.body);
    res.status(200).send(req.body);
});

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.set(PORT, process.argv[2] || process.env.APP_PORT || 3001);

app.listen(app.get(PORT) , function(){
    console.info("App Server started on " + app.get(PORT));
});
